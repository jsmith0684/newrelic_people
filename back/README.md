# README

This Ruby on Rails application was written with Ruby 2.5.0 and Ruby on Rails 5.1.5. I will assume Ruby and the Bundler gem
have already been installed. Run the command bundle install to install required gem libraries. Then run rails db:migrate to
create the SQLite database from the migrations files. Then run rails db:seed to seed the database with sample names. You are now
ready to run the Rails server. Run the command rails s -b 0.0.0.0. You can now issue requests to http://localhost:3000/api/v1/people

Unit tests can be run by issuing the command rails test.

