class Api::V1::PeopleController < Api::V1::ApiController
  before_action :set_person, only: [:update, :destroy]

  #
  # Return a list of users, which can be filtered by first and/or last name
  #
  # @param first-name: nil [string] first-name to filter list of people by
  # @param last-name: nil [string] last-name to filter list of people by
  #
  # @return [array] list of people
  def index
    @people = Person.search(first_name: params[:'first-name'], last_name: params[:'last-name'])
    render json: @people, status: :ok
  end

  #def show
  #  render json: @person, status: :ok
  #end

  #
  # Perist a new person object to database else return error
  #
  # @param first-name: nil [string] first-name of person object
  # @param last-name: nil [string] last-name of person object
  #
  # @return [person] Newly created person if saved in database successfully, else error
  def create
    @person = Person.new(person_params)
    if @person.save
      render json: @person, status: :created
    else
      render json: @person.errors, status: :unprocessable_entity
    end
  end

  #
  # Delete person from database by id
  #
  # @param id: nil [integer] id of person to remove from database
  #
  # @return [person] Deleted person object
  def destroy
    @person.destroy
  end

  #
  # Update person in database
  #
  # @param first-name: nil [string] first-name of person object
  # @param last-name: nil [string] last-name of person object
  #
  # @return [person] Updated person if changes successfully saved to database else error
  def update
    if @person.update(person_params)
      render json: @person
    else
      render json: @person.errors, status: :unprocessable_entity
    end
  end

  private
    def set_person
      @person = Person.find(params[:id])
    end

    def person_params
      ActiveModelSerializers::Deserialization.jsonapi_parse!(params, only: [:'first-name', :'last-name'])
    end
end
