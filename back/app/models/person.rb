class Person < ApplicationRecord
  validates :first_name, presence: true
  validates :last_name,  presence: true
  before_save :normalize_name

  #
  # Search for people by first and/or last name, if first and last name are nil then all people are returned
  # @param first_name: nil [string] first name of person
  # @param last_name: nil [string] last name of person
  #
  # @return [array] list of people
  def self.search(first_name: nil, last_name: nil)
    with_first_name(first_name)
      .with_last_name(last_name)
  end

  # filters search results by first and/or last name, if present
  scope :with_first_name, -> (first_name) { where('first_name like ?', "#{first_name}%") if first_name.present? }
  scope :with_last_name,  -> (last_name)  { where('last_name like ?', "#{last_name}%")   if last_name.present?  }


  #
  # Normalize first and last name by forcing capitalization for first letter
  #
  def normalize_name
    self.first_name = first_name.titleize
    self.last_name  = last_name.titleize
  end

end
