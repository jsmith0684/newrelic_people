Rails.application.routes.draw do

  namespace :api do
    namespace :v1 do
      resources :people, except: :show
    end
  end
end
