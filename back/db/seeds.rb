if Person.count == 0
  ["George Washington","John Adams","Thomas Jefferson","James Madison","James Monroe","John Adams","Andrew Jackson",
    "Martin VanBuren","William Harrison","John Tyler","James Polk","Zachary Taylor","Millard Fillmore","Franklin Pierce",
    "James Buchanan","Abraham Lincoln","Andrew Johnson","Ulysses Grant","Rutherford Hayes","James Garfield",
    "Chester Arthur","Grover Cleveland","Benjamin Harrison","Grover Cleveland","William McKinley","Theodore Roosevelt",
    "William Taft","Woodrow Wilson","Warren Harding","Calvin Coolidge","Herbert Hoover","Franklin Roosevelt",
    "Harry Truman"].each do |name|
    first, last = name.split
    Person.create(first_name: first, last_name: last)
  end
end
