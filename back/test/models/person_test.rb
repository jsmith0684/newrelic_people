require 'test_helper'

class PersonTest < ActiveSupport::TestCase
  def setup
    @p = people(:person1)
  end

  test 'person without last name' do
    @p.last_name = nil
    assert(@p.invalid?)
  end

  test 'person without first name' do
    @p.first_name = nil
    assert(@p.invalid?)
  end

  test 'search person by first name' do
    p = Person.search(first_name: 'bob')
    assert_equal(1, p.size())
    assert_equal('Dole', p[0].last_name)
  end

  test 'search person by last name' do
    p = Person.search(last_name: 'dole')
    assert_equal(1, p.size())
    assert_equal('Bob', p[0].first_name)
  end

  test 'search person by first and last name' do
    p = Person.search(first_name: 'bob', last_name: 'dole')
    assert_equal(1, p.size())
    assert_equal('Bob', p[0].first_name)
    assert_equal('Dole', p[0].last_name)
  end

  test 'normalize person name' do
    Person.create(first_name: 'jill', last_name: 'doe')
    p = Person.search(first_name: 'jill')
    assert_equal(1, p.size())
    assert_equal('Jill', p[0].first_name)
    assert_equal('Doe', p[0].last_name)
  end
end
