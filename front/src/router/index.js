// src/router/index.js

import Vue from 'vue'
import Router from 'vue-router'

import PersonSearchForm from '@/components/PersonSearchForm.vue'

Vue.use(Router)

const routes = [
  {path: '/', components: {default: PersonSearchForm}, name: 'people/index'}
]

export default new Router({
  routes: routes,
  mode: 'history'
})
