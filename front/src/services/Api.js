// src/services/Api.js

import axios from 'axios'

/**
 * Base uri for backend server
 * @return {[string]} base uri v1 of api
 */
export default () => {
  return axios.create({
    baseURL: `http://localhost:3000/api/v1/`
  })
}
