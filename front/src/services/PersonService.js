// src/services/PersonService.js

import Api from '@/services/Api'

export default {
  /**
   * Return a list of persons filtered by first and/or last name
   * @param  {[string]} firstName first name of person
   * @param  {[string]} lastName  last name of person
   * @return {[array]}           List of people filtered by names
   */
  getPersons (firstName, lastName) {
    return Api().get('people?first-name=' + firstName + '&last-name=' + lastName)
  },
  /**
   * Delete person by id
   * @param  {[integer]} id id of person to be removed from database
   */
  deletePerson (id) {
    return Api().delete('people/' + id)
  },
  /**
   * Create a new person object in database given a first and last name
   * @param  {[object]} data object containing attributes first-name and last-name
   * @return {[person]}      Newly created person object if successfuly, else error
   */
  createPerson (data) {
    return Api().post('people', data)
  }
}
