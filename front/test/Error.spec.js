import { mount } from 'avoriaz'
import { expect } from 'chai'
import Error from '@/components/Error.vue'

describe('Error.vue', () => {
  it('it has root element with class error', () => {
    const wrapper = mount(Error)
    expect(wrapper.is('.error')).to.equal(true)
  })

  it('it has alert class', () => {
    const wrapper = mount(Error)
    const alert = wrapper.find('.alert')[0]
    expect(alert.is('.alert')).to.equal(true)
  })

  it('slot should be empty', () => {
    const wrapper = mount(Error)
    const alert = wrapper.find('.alert')[0]
    expect(alert.text()).to.equal('')
  })

  //it('slot should contain text', () => {
  //  const error = 'this is an error'
  //  const wrapper = mount(Error, {
  //    slots: {
  //      foo: error
  //    }
  //  })
  //  const alert = wrapper.find('.alert')[0]
  //  expect(alert.text()).to.equal(error)
  //})
})
