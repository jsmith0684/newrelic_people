import { mount } from 'avoriaz'
import { expect } from 'chai'
import PeopleTable from '@/components/PeopleTable.vue'

describe('PeopleTable.vue', () => {
  it('it has root element with class people-table', () => {
    const wrapper = mount(PeopleTable)
    expect(wrapper.is('.people-table')).to.equal(true)
  })

  it('it should contain list of people', () => {
    const people = [{"id": "2","type": "people","attributes": {"first-name": "John","last-name": "Adams"}}]
    const wrapper = mount(PeopleTable, {propsData: {people}})
    expect(wrapper.propsData().people).to.equal(people)
  })
})
