import { mount } from 'avoriaz'
import { expect } from 'chai'
import PersonSearchForm from '@/components/PersonSearchForm.vue'

describe('PersonSearchForm.vue', () => {
  it('it has root element with class person-search-form', () => {
    const wrapper = mount(PersonSearchForm)
    expect(wrapper.is('.person-search-form')).to.equal(true)
  })

  it('it should support multiple error messages', () => {
    const wrapper = mount(PersonSearchForm)
    wrapper.setData({errors: ['error1', 'error2']})
    //const error = wrapper.find('.error')[0]
    expect(wrapper.data().errors).to.have.lengthOf(2)
    expect(wrapper.data().errors).to.include('error2')
  })

  //it('it should have onKeyPress function', () => {
  //  const wrapper = mount(PersonSearchForm)
  //  expect(typeof wrapper.watch().onKeyPress).to.equal('function')
  //})

  it('it should contain an empty list of people', () => {
    const wrapper = mount(PersonSearchForm)
    expect(wrapper.data().people).to.be.empty
  })

  it('it should contain an onKeyPress empty property', () => {
    const wrapper = mount(PersonSearchForm)
    expect(wrapper.data().onKeyPress).to.be.empty
  })
})
